import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_boom_menu/flutter_boom_menu.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MaterialApp(
    home: ContactScreen(),
  ));
}

class ContactScreen extends StatefulWidget {
  ContactScreen({Key key}) : super(key: key);
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {

  Completer<GoogleMapController> _controller = Completer();
  bool scrolVisible = true;
  ScrollController scrollController;

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController()
    ..addListener((){
      setDialVisible(scrollController.position.userScrollDirection == ScrollDirection.forward);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
       child: Scaffold(
         body: GoogleMap(
           mapType: MapType.hybrid,
           initialCameraPosition: _myPosition,
           onMapCreated: (GoogleMapController controller) {
             _controller.complete(controller);
           },
         ),
         floatingActionButton: buildBoomMenu(),
       ),
    );
  }

  void setDialVisible(bool value){
    setState(() {
      scrolVisible = value;
    });
  }

  BoomMenu buildBoomMenu() {
    return BoomMenu(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      overlayColor: Colors.black,
      overlayOpacity: 0.7,
      scrollVisible: scrolVisible,
      children: [
        MenuItem(
          child: Icon(MdiIcons.gmail),
          title: 'Email',
          titleColor: Colors.green[850],
          subtitle: 'krypdev@gmail.com',
          subTitleColor: Colors.grey[850],
          backgroundColor: Colors.blue[50],
          onTap: () => _sendEmail()
        ),
        MenuItem(
          child: Icon(MdiIcons.phone),
          title: 'Phone number',
          titleColor: Colors.green[850],
          subtitle: '4616453985',
          subTitleColor: Colors.grey[850],
          backgroundColor: Colors.blue[50],
          onTap: () => _launchCallPhone()
        ),
        MenuItem(
          child: Icon(MdiIcons.whatsapp),
          title: 'Message',
          titleColor: Colors.green[850],
          subtitle: '4616453985',
          subTitleColor: Colors.grey[850],
          backgroundColor: Colors.blue[50],
          onTap: () => _sendMessage()
        ),
      ],
    );
  }

  _sendEmail()async{
    final Uri params = Uri(
    scheme: 'mailto',
    path: 'krypdev@gmail.com',
    query: 'subject=Contacto App&body=Buen dia, el motivo del presente....'
    );
    var email = params.toString();
    if(await canLaunch(email)){
      await launch(email);
    }
  }

  _launchCallPhone() async{
    const tel='tel:4616453985';
    if(await canLaunch(tel)){
      await launch(tel);
    }
  }

  _sendMessage()async{
    const tel='sms:4616453985';
    if(await canLaunch(tel)){
      await launch(tel);
    }
  }

  CameraPosition _myPosition = CameraPosition(
    target: LatLng(20.541763, -100.812309),
    zoom: 25.0
  );
}